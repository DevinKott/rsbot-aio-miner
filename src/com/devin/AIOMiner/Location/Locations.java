package com.devin.AIOMiner.location;

import java.util.ArrayList;
import java.util.List;

import com.devin.AIOMiner.areas.banks.Banks;
import com.devin.AIOMiner.areas.mines.Mines;

public class Locations {

	public static Location EAST_VARROCK = new Location(Banks.EAST_VARROCK,
			Mines.EAST_VARROCK);

	public static List<Location> locations = new ArrayList<Location>();

	public void initLocations() {
		locations.add(EAST_VARROCK);
	}

	public static Location EAST_VARROCK() {
		return EAST_VARROCK;
	}
}
