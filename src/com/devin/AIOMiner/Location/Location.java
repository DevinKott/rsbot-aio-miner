package com.devin.AIOMiner.location;

import com.devin.AIOMiner.areas.banks.Bank;
import com.devin.AIOMiner.areas.mines.Mine;

public class Location {

	private Bank bank;
	private Mine mine;
	
	public Location(final Bank bank, final Mine area) {
		this.bank = bank;
		this.mine = area;
	}
	
	public Bank getBank() {
		return bank;
	}
	
	public Mine getMine() {
		return mine;
	}
}
