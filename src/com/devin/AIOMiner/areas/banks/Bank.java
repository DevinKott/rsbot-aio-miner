package com.devin.AIOMiner.areas.banks;

import org.powerbot.script.Area;
import org.powerbot.script.Tile;

public class Bank {

	private Area area;
	private Tile bankTile;
	
	public Bank(Area bankArea, Tile tile) {
		this.area = bankArea;
		this.bankTile = tile;
	}
	
	/**
	 * Returns the area of the bank. Used for checks to see if you can see/reach the bank tile
	 */
	public Area getArea() {
		return area;
	}
	
	/**
	 * Returns the bank tile that the character should walk towards. Once at the tile, the character can bank
	 */
	public Tile getBankTile() {
		return bankTile;
	}
}
