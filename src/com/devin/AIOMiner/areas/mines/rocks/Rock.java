package com.devin.AIOMiner.areas.mines.rocks;

import org.powerbot.script.Tile;

public class Rock {

	private Tile preferredTile;
	private int rockObjectID;
	
	public Rock(final Tile pre, final int id) {
		preferredTile = pre;
		rockObjectID = id;
	}
	
	public Tile getTile() {
		return preferredTile;
	}
	
	public int getId() {
		return rockObjectID;
	}
}
