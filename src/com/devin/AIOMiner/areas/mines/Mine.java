package com.devin.AIOMiner.areas.mines;

import org.powerbot.script.Area;

import com.devin.AIOMiner.areas.mines.rocks.Rock;

public class Mine {

	private Area area;
	private Rock[] rocks;
	private int[] rockItemIds;
	
	public Mine(final Area area, final Rock[] rocks, final int[] rockItems) {
		this.area = area;
		this.rocks = rocks;
		this.rockItemIds = rockItems;
	}
	
	public Area getArea() {
		return area;
	}
	
	public Rock[] getRocks() {
		return rocks;
	}
	
	public int[] getRockItemIds() {
		return rockItemIds;
	}
}
