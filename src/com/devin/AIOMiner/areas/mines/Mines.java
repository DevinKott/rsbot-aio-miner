package com.devin.AIOMiner.areas.mines;

import java.util.ArrayList;
import java.util.List;

import org.powerbot.script.Area;
import org.powerbot.script.Tile;

import com.devin.AIOMiner.areas.mines.rocks.Rock;

public class Mines {

	public static List<Mine> mines = new ArrayList<Mine>();
	public static Mine EAST_VARROCK = new Mine(new Area(
			new Tile(3274, 3373, 0), new Tile(3275, 3356, 0), new Tile(3297,
					3356, 0), new Tile(3294, 3373, 0)), new Rock[] {
			new Rock(new Tile(3283, 3368, 0), 11962),
			new Rock(new Tile(3283, 3369, 0), 11960),
			new Rock(new Tile(3286, 3368, 0), 11955),
			new Rock(new Tile(3286, 3368, 0), 11956),
			new Rock(new Tile(3285, 3370, 0), 11954),
			new Rock(new Tile(3287, 3370, 0), 11956),
			new Rock(new Tile(3288, 3368, 0), 11959),
			new Rock(new Tile(3287, 3366, 0), 11957),
			new Rock(new Tile(3287, 3366, 0), 11961),
			new Rock(new Tile(3286, 3365, 0), 11960),
			new Rock(new Tile(3289, 3362, 0), 11961),
			new Rock(new Tile(3289, 3362, 0), 11962),
			new Rock(new Tile(3288, 3362, 0), 11960),
			new Rock(new Tile(3287, 3362, 0), 11962),
			new Rock(new Tile(3285, 3362, 0), 11961),
			new Rock(new Tile(3282, 3362, 0), 11958),
			new Rock(new Tile(3282, 3363, 0), 11957),
			new Rock(new Tile(3282, 3363, 0), 11957),
			new Rock(new Tile(3283, 3365, 0), 11959), }, new int[] { 438, 439,
			436, 437, 440, 441, 20903 });

	public void initMines() {
		mines.add(EAST_VARROCK);
	}
}
