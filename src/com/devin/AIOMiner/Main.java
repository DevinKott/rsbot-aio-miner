package com.devin.AIOMiner;

import java.util.Arrays;

import org.powerbot.script.Script;
import org.powerbot.script.rt6.ClientContext;

import com.devin.AIOMiner.task.M1D1;

@Script.Manifest(description = "AIO Miner <--", name = "AIO Miner", properties = "client=6;")
public class Main extends GraphScript<ClientContext> {

	public Main() {
		chain.addAll(Arrays.asList(new M1D1(ctx)));
	}
}
