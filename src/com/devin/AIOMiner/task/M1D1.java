package com.devin.AIOMiner.task;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt6.Backpack;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.GameObject;
import org.powerbot.script.rt6.Item;
import org.powerbot.script.rt6.Hud.Window;

import com.devin.AIOMiner.GraphScript;
import com.devin.AIOMiner.areas.banks.Bank;
import com.devin.AIOMiner.areas.banks.Banks;
import com.devin.AIOMiner.areas.mines.Mines;
import com.devin.AIOMiner.areas.mines.rocks.Rock;
import com.devin.AIOMiner.location.Location;
import com.devin.AIOMiner.location.Locations;

public class M1D1 extends GraphScript.Action<ClientContext> {

	public M1D1(ClientContext ctx) {
		super(ctx);
	}

	Bank bank = null;
	Location location = null;

	Banks banks = new Banks();
	Mines mines = new Mines();
	Locations locs = new Locations();

	@Override
	public void run() {
		if (location == null) {
			banks.initBanks();
			mines.initMines();
			locs.initLocations();
			location = Locations.EAST_VARROCK();
		}
		if (!location.getMine().getArea().contains(ctx.players.local())) {
			// walk to mine
		} else {
			Rock[] rocks = location.getMine().getRocks();
			int[] rockIds = new int[rocks.length];
			for (int i = 0; i < rockIds.length; i++) {
				rockIds[i] = rocks[i].getId();
			}
			if (ctx.players.local().idle()) {
				drop(location.getMine().getRockItemIds());
				mine(rockIds);
			}
		}
	}

	public void drop(final int[] ids) {
		if (!ctx.hud.opened(Window.BACKPACK))
			ctx.hud.open(Window.BACKPACK);
		for (Item item : ((Backpack) ctx.backpack.select()).items()) {
			int itemID = item.id();
			for (int i = 0; i < ids.length; i++) {
				int toDrop = ids[i];
				if (itemID == toDrop) {
					item.interact("Drop");
				}
			}
		}
	}

	public void mine(final int[] ids) {
		GameObject rock = ctx.objects.select().id(ids).within(5).poll();
		rock.interact("Mine");
		Condition.sleep(500);
		Condition.wait(new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception {
				return ctx.players.local().animation() != -1;
			}

		});
	}

	@Override
	public boolean valid() {
		return true;
	}
}
