package com.devin.AIOMiner.AI;

import org.powerbot.script.rt4.ClientContext;

import com.devin.AIOMiner.location.Location;
import com.devin.AIOMiner.location.Locations;

public class FindLocation {

	public Location getLocation(ClientContext ctx) {
		for (Location location : Locations.locations) {
			if (location.getMine().getArea().contains(ctx.players.local()))
				return location;
		}
		return new Location(null, null);
	}
}
